use std::{
    env,
    error::Error,
    fs,
    io::{self, Write},
    path::PathBuf,
};

use clap::{Parser, ValueEnum};
use env_logger::Env;
use log::error;

#[derive(Parser)]
struct Cli {
    /// twitch auth token, will ask for token if it isnt saved
    #[arg(long)]
    token: Option<String>,
    /// path to saved token, must be file
    #[arg(long, default_value = "$HOME/.local/share/twit/token", value_parser = path_env)]
    token_path: PathBuf,
    /// show token, dont do that on stream
    #[arg(long)]
    show_token: bool,
    /// channels to view chat on, whitespace seperated
    channels: String,
    /// filter incoming messages
    ///
    /// twitch uses irc messages to comunicate with bot
    /// so this is more than just user messages, only user messages by default
    #[arg(short, long, default_value = "message")]
    filter: Vec<TwitchCode>,
    /// prints all messages with extra detail, disregards filter
    #[arg(short, long)]
    debug: bool,
}

fn path_env(s: &str) -> Result<PathBuf, Box<dyn Error + Send + Sync + 'static>> {
    let mut s = s.to_string();
    while let Some(i) = s.find('$') {
        let a = &s[i + 1..];
        if let Some(e) = a.find(|c: char| !(c.is_ascii_alphanumeric() || c == '_')) {
            let var = env::var(&a[..e])?;
            s.replace_range(i..=e, &var);
        }
    }
    Ok(PathBuf::from(s))
}

#[derive(Debug, PartialEq, Eq, ValueEnum, Clone)]
enum TwitchCode {
    Notice,
    UserList,
    EndUserList,
    // used for codes that this enum doesnt define, ie codes that arent used
    Message,
    UnknownCommand,
    Undefined,
}

impl From<&str> for TwitchCode {
    fn from(value: &str) -> Self {
        match value.trim() {
            "NOTICE" => TwitchCode::Notice,
            "PRIVMSG" => TwitchCode::Message,
            "421" => TwitchCode::UnknownCommand,
            "353" => TwitchCode::UserList,
            "366" => TwitchCode::EndUserList,
            _ => TwitchCode::Undefined,
        }
    }
}

#[allow(dead_code)]
struct TwitchMessage<'a> {
    user: &'a str,
    server: &'a str,
    channel: &'a str,
    code: TwitchCode,
    code_str: &'a str,
    body: &'a str,
}

impl<'a> TwitchMessage<'a> {
    fn parse_messages(s: &str) -> Vec<TwitchMessage<'_>> {
        let m = s.split("\r\n");
        let mut messages = vec![];
        for s in m {
            if !s.starts_with(':') {
                break;
            }
            let Some((message_data, body)) = s.trim_start_matches(':').split_once(':') else {
                continue;
            };
            let (server, message_data) = message_data.trim().split_once(' ').unwrap();
            let (user, _server) = server.split_once('!').unwrap_or(("", server));
            let (code, channel) = message_data.split_once(' ').unwrap();
            messages.push(TwitchMessage {
                server,
                user,
                channel,
                code: code.into(),
                code_str: code,
                body,
            })
        }
        messages
    }
}

fn display_messages(m: &[TwitchMessage], f: &[TwitchCode]) {
    for m in m {
        if f.contains(&m.code) {
            println!("[{}] {}", m.user, m.body);
        }
    }
}

fn debug_messages(m: &[TwitchMessage]) {
    for m in m {
        println!(
            "{} {} [{}] <{}> {}",
            m.server, m.channel, m.user, m.code_str, m.body
        );
    }
}

fn main() {
    env_logger::init_from_env(
        Env::new()
            .filter_or("TWIT_LOG", "twit=trace")
            .write_style_or("TWIT_LOG_STYLE", "always"),
    );

    let cli = Cli::parse();

    let token = if let Some(p) = cli.token {
        p
    } else if cli.token_path.exists() {
        fs::read_to_string(cli.token_path).expect("could not read token")
    } else {
        println!("open https://id.twitch.tv/oauth2/authorize?response_type=token&client_id=1o1ctjbpk6gaajexpgd6fnz4w9kqy5&redirect_uri=http://localhost:3000&scope=chat%3Aread+chat%3Aedit");
        print!("paste access_token > ");
        io::stdout().flush().unwrap();
        let mut token = String::new();
        io::stdin().read_line(&mut token).unwrap();
        if let Some(p) = cli.token_path.parent() {
            if let Err(e) = fs::create_dir_all(p) {
                error!("couldnt create dir {e}");
            };
        }
        if let Err(e) = fs::write(cli.token_path, &token) {
            error!("couldnt save token {e}");
        };
        token
    };
    if cli.show_token {
        println!("token: {token}");
    }

    let (mut client, _) = tungstenite::connect("ws://irc-ws.chat.twitch.tv:80").expect("jlj");

    if let Err(e) = client.send(format!("PASS oauth:{token}").into()) {
        error!("couldnt send token {e}");
    };
    if let Err(e) = client.send("NICK twit".into()) {
        error!("couldnt set nick {e}");
        return;
    };
    if let Ok(m) = client.read() {
        match m {
            tungstenite::Message::Text(t) => {
                let m = &TwitchMessage::parse_messages(&t);
                if cli.debug {
                    debug_messages(m);
                } else {
                    display_messages(m, &cli.filter);
                }
            }
            _ => error!("expected text, got something else"),
        }
    }

    let mut channels = String::new();
    for c in cli.channels.split_whitespace() {
        if !channels.is_empty() {
            channels.push(',');
        }
        channels.push('#');
        channels.push_str(c);
    }

    if let Err(e) = client.send(format!("JOIN {}", channels).into()) {
        error!("couldnt join channel {e}");
        return;
    };

    if let Ok(m) = client.read() {
        match m {
            tungstenite::Message::Text(t) => {
                let m = &TwitchMessage::parse_messages(&t);
                if cli.debug {
                    debug_messages(m);
                } else {
                    display_messages(m, &cli.filter);
                }
            }
            _ => error!("expected text, got something else"),
        }
    }
    loop {
        if let Ok(m) = client.read() {
            match m {
                tungstenite::Message::Text(t) => {
                    let m = &TwitchMessage::parse_messages(&t);
                    if cli.debug {
                        debug_messages(m);
                    } else {
                        display_messages(m, &cli.filter);
                    }
                }
                _ => error!("expected text, got something else"),
            }
        }
    }
}
